package main

import (
	"fmt"
	"os"
	"strings"
	"strconv"
	"sort"
)

func main() {
	input, err := os.ReadFile("./input.txt")
	if err != nil {
		panic(err)
	}

	elves := make([]int, 0)
	data := strings.Split(string(input), "\n")

	var total int
	for _, line := range data {
		if line == "" {
			elves = append(elves, total)
			total = 0
			continue
		}

		calories, err := strconv.ParseInt(line, 10, 32)
		if err != nil {
			panic(err)
		}

		total += int(calories)
	}

	sort.Ints(elves)
	sort.Sort(sort.Reverse(sort.IntSlice(elves)))

	fmt.Println("Answer 1", elves[0])
	fmt.Println("Answer 2", elves[0] + elves[1] + elves[2])
}
