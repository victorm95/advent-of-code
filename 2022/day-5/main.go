package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Operation struct {
	Move int
	From int
	To   int
}

type Stack struct {
	data []string
}

func (s *Stack) Pop() (string, error) {
	if len(s.data) == 0 {
		return "", errors.New("Stack is empty")
	}

	idx := len(s.data) - 1
	last := s.data[idx]
	tmp := make([]string, idx)

	for i := range tmp {
		tmp[i] = s.data[i]
	}
	s.data = tmp

	return last, nil
}

func (s *Stack) Push(crate string) {
	s.data = append(s.data, crate)
}

func (s *Stack) Swap(to *Stack, amount int) {
	tmp := new(Stack)

	for i := 0; i < amount; i++ {
		el, err := s.Pop()
		if err != nil {
			break
		}

		tmp.Push(el)
	}

	for i := 0; i < amount; i++ {
		el, err := tmp.Pop()
		if err != nil {
			break
		}

		to.Push(el)
	}
}

func main() {
	input, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	crates, ops := parse(string(input))

	for _, op := range ops {
		from := crates[op.From]
		to := crates[op.To]

		from.Swap(to, op.Move)
	}

	var result strings.Builder
	for i := 1; i <= len(crates); i++ {
		val, _ := crates[i].Pop()
		result.WriteString(val)
	}

	fmt.Println(result.String())
}

func parse(input string) (map[int]*Stack, []Operation) {
	var crates strings.Builder
	var ops strings.Builder

	readingOps := false
	for _, line := range strings.Split(input, "\n") {
		if line == "" {
			readingOps = true
			continue
		}

		if readingOps {
			ops.WriteString(line)
			ops.WriteRune('\n')
		} else {
			crates.WriteString(line)
			crates.WriteRune('\n')
		}
	}

	return parseCrates(crates.String()), parseOps(ops.String())
}

func parseCrates(input string) map[int]*Stack {
	crates := make(map[int]*Stack)
	re, _ := regexp.Compile(`\d`)

	var idx []int
	var keys string
	for _, line := range strings.Split(input, "\n") {
		if matches := re.FindAllStringIndex(line, -1); matches != nil {
			keys = line
			for _, match := range matches {
				idx = append(idx, match[0])
			}
		}
	}

	for _, i := range idx {
		key, _ := strconv.Atoi(keys[i : i+1])
		crates[key] = new(Stack)
	}

	for _, line := range strings.Split(input, "\n") {
		if re.MatchString(line) {
			break
		}

		for _, i := range idx {
			key, _ := strconv.Atoi(keys[i : i+1])
			val := line[i : i+1]

			if val == " " {
				continue
			}

			crates[key].data = append([]string{val}, crates[key].data...)
		}
	}

	return crates
}

func parseOps(input string) []Operation {
	var ops []Operation
	re, _ := regexp.Compile(`\d+`)
	for _, line := range strings.Split(input, "\n") {
		data := re.FindAllString(line, -1)
		if data == nil {
			break
		}

		amount, _ := strconv.Atoi(data[0])
		from, _ := strconv.Atoi(data[1])
		to, _ := strconv.Atoi(data[2])
		ops = append(ops, Operation{Move: amount, From: from, To: to})
	}

	return ops
}
