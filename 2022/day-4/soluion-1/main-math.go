// Part 1 Math solution
package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Range struct {
	From int
	To   int
}

func main() {
	input, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	var total int
	for _, line := range strings.Split(string(input), "\n") {
		if line == "" {
			continue
		}

		pair := strings.Split(line, ",")
		one := getRange(pair[0])
		two := getRange(pair[1])

		if fullyContains(one, two) {
			total++
		}
	}

	fmt.Println(total)
}

func getRange(input string) Range {
	rang := strings.Split(input, "-")
	from, _ := strconv.Atoi(rang[0])
	to, _ := strconv.Atoi(rang[1])

	return Range{From: from, To: to}
}

func fullyContains(one, two Range) bool {
	return (one.From >= two.From && one.To <= two.To) ||
		(two.From >= one.From && two.To <= one.To)
}
