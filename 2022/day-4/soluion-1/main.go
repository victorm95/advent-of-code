// Part 1
package main

import (
	"fmt"
	"os"
	"strings"
	"strconv"
)

func main() {
	input, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	var total int
	for _, line := range strings.Split(string(input), "\n") {
		if line == "" {
			continue
		}

		fmt.Println("Line:", line)
		pair := strings.Split(line, ",")
		oneFrom, oneTo := getRange(pair[0])
		twoFrom, twoTo := getRange(pair[1])

		if fullyContains(genSections(oneFrom, oneTo), genSections(twoFrom, twoTo)) {
			total++
		}
	}

	fmt.Println(total)
}

func getRange(input string) (int, int) {
	fmt.Println("Range of", input)
	rang := strings.Split(input, "-")
	from, _ := strconv.Atoi(rang[0])
	to, _ := strconv.Atoi(rang[1])

	return from, to
}

func genSections(from, to int) string {
	var builder strings.Builder
	builder.WriteRune('.')
	for i := from; i <= to; i++ {
		builder.WriteString(strconv.Itoa(i))
		builder.WriteRune('.')
	}
	return builder.String()
}

func fullyContains(a, b string) bool {
	return strings.Contains(a, b) || strings.Contains(b, a)
}
