package main

import (
	"fmt"
	"os"
)

const MARKER_LEN = 14

type Set struct {
	data []string
}

func (s *Set) Add(element string) {
	for _, el := range s.data {
		if element == el {
			return
		}
	}
	s.data = append(s.data, element)
}

func (s *Set) Len() int {
	return len(s.data)
}

func (s *Set) Reset() {
	s.data = make([]string, 0)
}

func main() {
	input, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	data := string(input)

	set := new(Set)
	for i := 0; i < len(data)-MARKER_LEN; i++ {
		marker := data[i : i+MARKER_LEN]
		for j := 0; j < MARKER_LEN; j++ {
			set.Add(marker[j : j+1])
		}

		if set.Len() == MARKER_LEN {
			fmt.Println(i + MARKER_LEN)
			break
		}

		set.Reset()
	}
}
