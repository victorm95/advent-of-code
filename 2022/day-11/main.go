package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

const (
	FILE = "test.txt"
)

var numRegexp *regexp.Regexp
var signRegexp *regexp.Regexp
var operValueRegexp *regexp.Regexp

var monkeys []*Monkey

type Monkey struct {
	Inspected int
	Items     []int
	Operation struct {
		Sign  string
		Value int
	}
	Test struct {
		Divisible int
		True      int
		False     int
	}
}

func init() {
	var err error

	numRegexp, err = regexp.Compile(`\d+`)
	if err != nil {
		panic(err)
	}

	signRegexp, err = regexp.Compile(`(\+|\*)`)
	if err != nil {
		panic(err)
	}

	operValueRegexp, err = regexp.Compile(`(old|\d+)$`)
	if err != nil {
		panic(err)
	}
}

func init() {
	input, err := os.ReadFile(FILE)
	if err != nil {
		panic(err)
	}

	data := strings.Split(string(input), "\n\n")

	for _, block := range data {
		monkey := new(Monkey)
		lines := strings.Split(block, "\n")

		// Items
		for _, item := range numRegexp.FindAllString(lines[1], -1) {
			value, _ := strconv.Atoi(item)
			monkey.Items = append(monkey.Items, value)
		}

		// Operation
		monkey.Operation.Sign = signRegexp.FindString(lines[2])
		monkey.Operation.Value, _ = strconv.Atoi(operValueRegexp.FindString(lines[2]))

		// Test
		monkey.Test.Divisible, _ = strconv.Atoi(numRegexp.FindString(lines[3]))
		monkey.Test.True, _ = strconv.Atoi(numRegexp.FindString(lines[4]))
		monkey.Test.False, _ = strconv.Atoi(numRegexp.FindString(lines[5]))

		monkeys = append(monkeys, monkey)
	}
}

func main() {
	var oper func(int, int) float64

	for round := 0; round < 20; round++ {
		for _, monkey := range monkeys {
			if monkey.Operation.Sign == "+" {
				oper = sum
			} else {
				oper = mul
			}

			for _, item := range monkey.Items {
				monkey.Inspected++
				var index int
				value := int(math.Floor(oper(item, monkey.Operation.Value) / 3))

				if value%monkey.Test.Divisible == 0 {
					index = monkey.Test.True
				} else {
					index = monkey.Test.False
				}

				monkeys[index].Items = append(monkeys[index].Items, value)
			}

			monkey.Items = nil
		}
	}

	sort.Slice(monkeys, func(i, j int) bool {
		return monkeys[i].Inspected > monkeys[j].Inspected
	})

	for _, m := range monkeys {
		fmt.Println(m.Inspected)
	}
}

func sum(a, b int) float64 {
	if b == 0 {
		return float64(a + a)
	}
	return float64(a + b)
}

func mul(a, b int) float64 {
	if b == 0 {
		return float64(a * a)
	}
	return float64(a * b)
}
