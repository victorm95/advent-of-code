#lang racket

(define grid (make-hash))
(define grid2 (make-hash))
(define starting-point #f)
(define ending-point #f)
(define posible-ending-points '())
(define width 0)
(define height 0)


(call-with-input-file "input.txt"
  (λ (in)
    (for ([line (in-list (regexp-split "\n" in))]
          [i (in-naturals)]
          #:when #t
          [char (in-string (bytes->string/utf-8 line))]
          [j (in-naturals)])
      (let ([key (cons i j)]
            [is-starting-point? (equal? char #\E)]
            [is-ending-point? (equal? char #\S)])
        (set! width (max width j))
        (set! height (max height i))
        (when is-starting-point?
          (set! starting-point key))
        (when is-ending-point?
          (set! ending-point key))
        (when (or (equal? char #\a)
                  (equal? char #\S))
          (set! posible-ending-points (cons key posible-ending-points)))
        (hash-set! grid
                   (cons i j)
                   (cons (char->integer (match char
                                          [#\S #\a]
                                          [#\E #\z]
                                          [_ char]))
                         (if is-starting-point? 0 #f)))
        (hash-set! grid2
                   (cons i j)
                   (cons (char->integer (match char
                                          [#\S #\a]
                                          [#\E #\z]
                                          [_ char]))
                         (if is-starting-point? 0 #f)))))))


(define (gen-keys key)
  (let ([x (cdr key)]
        [y (car key)])
    (list (cons (add1 y) x)
          (cons (sub1 y) x)
          (cons y (add1 x))
          (cons y (sub1 x)))))


(define (can-climb? current next)
  (let ([next (hash-ref grid next #f)])
    (if (not next)
        #f
        (< (- (car (hash-ref grid current))
              (car next))
           2))))


(define (print-grid)
  (for* ([i (in-range (add1 height))]
         [j (in-range (add1 width))])
    (display (if (cdr (hash-ref grid (cons i j)))
                 "#"
                 "."))          
    (when (= j width)
      (printf "\n")))
  (display (list->string (make-list (+ 5 width) #\-)))
  (display "\n"))


;; Solution 1
(let loop ([keys (list starting-point)])
  ;; (print-grid)
  (if (member ending-point keys)
      (cdr (hash-ref grid ending-point))
      (loop (for*/fold ([result '()])
                       ([key (in-list keys)]
                        [next (in-list (gen-keys key))]
                        #:when (can-climb? key next))
              (let ([curr (hash-ref grid key)]
                    [nxt (hash-ref grid next)])
                (if (cdr nxt)
                    result
                    (begin
                      (hash-set! grid next (cons (car nxt)
                                                 (add1 (cdr curr))))
                      (cons next result))))))))
  

;; Solution 2
(let loop ([keys (list starting-point)])
  (let ([finished? (for/first ([key (in-list posible-ending-points)]
                               #:when (cdr (hash-ref grid2 key)))
                     (hash-ref grid2 key))])
    (if finished?
        (cdr finished?)
        (loop (for*/fold ([result '()])
                         ([key (in-list keys)]
                          [next (in-list (gen-keys key))]
                          #:when (can-climb? key next))
                (let ([curr (hash-ref grid2 key)]
                      [nxt (hash-ref grid2 next)])
                  (if (cdr nxt)
                      result
                      (begin
                        (hash-set! grid2 next (cons (car nxt)
                                                    (add1 (cdr curr))))
                        (cons next result)))))))))