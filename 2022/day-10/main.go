package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

const FILE = "input.txt"

var program []string

func init() {
	input, err := os.ReadFile(FILE)

	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(input), "\n") {
		if line == "" {
			break
		}
		program = append(program, line)
	}
}

func main() {
	var cicle int
	var register int
	var builder strings.Builder
	var sprite string

	register = 1
	sprite = "###....................................."
	for i, writing := 0, false; i < len(program); {
		index := cicle % 40
		builder.WriteString(sprite[index : index+1])
		if index == 39 {
			builder.WriteRune('\n')
		}

		cicle++
		instruction := program[i]

		if instruction == "noop" {
			i++
			continue
		}

		data := strings.Split(instruction, " ")
		x, _ := strconv.Atoi(data[1])

		if writing {
			register += x
			i++
			writing = false
			sprite = calcSprite(register)
		} else {
			writing = true
		}
	}

	fmt.Println(builder.String())
}

func calcSprite(x int) string {
	var builder strings.Builder

	for i := 1; i <= 40; i++ {
		if i >= x && i <= x +2 {
			builder.WriteRune('#')
		} else {
			builder.WriteRune('.')
		}
	}

	return builder.String()
}
