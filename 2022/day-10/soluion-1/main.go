package main

import (
	"fmt"
	"os"
	"strings"
	"strconv"
)

const FILE = "input.txt"

var program []string

func init() {
	input, err := os.ReadFile(FILE)

	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(input), "\n") {
		if line == "" {
			break
		}
		program = append(program, line)
	}
}

func main() {
	var cicle int = 1
	var total int
	prev := 1
	register := 1
	pickCicle := 20

	for _, instruction := range program {
		if cicle == pickCicle {
			total += pickCicle * register
			pickCicle += 40
		} else if cicle > pickCicle {
			total += pickCicle * prev
			pickCicle += 40
		}

		if instruction == "noop" {
			cicle++
			continue
		}

		data := strings.Split(instruction, " ")
		x, _ := strconv.Atoi(data[1])

		prev = register
		register += x
		cicle += 2
	}

	fmt.Println(total)
}
