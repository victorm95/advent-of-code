package main

import (
	"fmt"
	"os"
	"strings"
)

const (
	Rock     rune = 'A'
	Paper    rune = 'B'
	Scissors rune = 'C'

	Lose rune = 'X'
	Draw rune = 'Y'
	Win  rune = 'Z'

	ShapeRock     int = 1
	ShapePaper    int = 2
	ShapeScissors int = 3

	OutcomeLost int = 0
	OutcomeDraw int = 3
	OutcomeWon  int = 6
)

func main() {
	input, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	data := strings.Split(string(input), "\n")

	var totalScore int
	for _, line := range data {
		if line == "" {
			continue
		}

		match := []rune(line)
		totalScore += playMatch(match[0], match[2])
	}

	fmt.Println(totalScore)
}

func playMatch(opponent, need rune) int {
	var shape int
	var outcome int

	switch need {
	case Lose:
		outcome = OutcomeLost
		switch opponent {
		case Rock:
			shape = ShapeScissors

		case Paper:
			shape = ShapeRock

		case Scissors:
			shape = ShapePaper
		}

	case Draw:
		outcome = OutcomeDraw
		switch opponent {
		case Rock:
			shape = ShapeRock

		case Paper:
			shape = ShapePaper

		case Scissors:
			shape = ShapeScissors
		}

	case Win:
		outcome = OutcomeWon
		switch opponent {
		case Rock:
			shape = ShapePaper

		case Paper:
			shape = ShapeScissors

		case Scissors:
			shape = ShapeRock
		}
	}

	return shape + outcome
}
