package main

import (
	"fmt"
	"os"
	"strings"
)

var priorities map[rune]int

func init() {
	priorities = make(map[rune]int)

	for i := 1; i <= 26; i++ {
		key := []rune(string(i+96))[0]
		priorities[key] = i
	}

	for i := 27; i <= 52; i++ {
		key := []rune(string(i+38))[0]
		priorities[key] = i
	}
}

func main() {
	input, err := os.ReadFile("input.txt")
	if err != err {
		panic(err)
	}

	data := string(input)
	backpacks := strings.Split(data, "\n")

	var total int
	var elves []string
	for _, backpack := range backpacks {
		elves = append(elves, backpack)

		if len(elves) == 3 {
			repeated := findRepeated(elves)
			total += priorities[repeated]
			elves = make([]string, 0)
		}
	}

	fmt.Println(total)
}

func findRepeated(elves []string) rune {
	for _, char := range elves[0] {
		if strings.Contains(elves[1], string(char)) && strings.Contains(elves[2], string(char)) {
			return char
		}
	}
	return '-'
}
