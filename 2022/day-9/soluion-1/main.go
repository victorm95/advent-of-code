package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

const INPUT = "input.txt"

type Status byte

const (
	HeadVisited Status = iota
	BothVisited
)

const (
	Up    = "U"
	Left  = "L"
	Down  = "D"
	Right = "R"
)

var motions []Motion
var grid map[string]Status

type Motion struct {
	Direction string
	Steps     int
}

type Position struct {
	X int
	Y int
}

func (p *Position) Key() string {
	return fmt.Sprintf("%d,%d", p.X, p.Y)
}

func (p *Position) Distance(pos Position) Position {
	return Position{
		X: pos.X - p.X,
		Y: pos.Y - p.Y,
	}
}

func init() {
	input, err := os.ReadFile(INPUT)
	if err != nil {
		panic(err)
	}
	parse(string(input))
}

func init() {
	grid = make(map[string]Status)
	grid["0,0"] = BothVisited
}

func main() {
	head := new(Position)
	tail := new(Position)

	for _, motion := range motions {
		for i := 0; i < motion.Steps; i++ {
			switch motion.Direction {
			case Left:
				head.X--
			case Right:
				head.X++
			case Up:
				head.Y++
			case Down:
				head.Y--
			}

			if _, ok := grid[head.Key()]; !ok {
				grid[head.Key()] = HeadVisited
			}

			distance := tail.Distance(*head)
			switch {
			case distance.X == 2:
				tail.X++
				if distance.Y != 0 {
					tail.Y = head.Y
				}

			case distance.X == -2:
				tail.X--
				if distance.Y != 0 {
					tail.Y = head.Y
				}

			case distance.Y == 2:
				tail.Y++
				if distance.X != 0 {
					tail.X = head.X
				}

			case distance.Y == -2:
				tail.Y--
				if distance.X != 0 {
					tail.X = head.X
				}
			}

			grid[tail.Key()] = BothVisited
		}
	}

	var total int
	for _, status := range grid {
		if status == BothVisited {
			total ++
		}
	}
	fmt.Println(total)
}

func parse(input string) {
	for _, line := range strings.Split(input, "\n") {
		if line == "" {
			break
		}
		data := strings.Split(line, " ")
		steps, _ := strconv.Atoi(data[1])
		motions = append(motions, Motion{Direction: data[0], Steps: steps})
	}
}
