package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

const INPUT = "input.txt"

var trees [][]int

func init() {
	input, err := os.ReadFile(INPUT)
	if err != err {
		panic(err)
	}
	trees = format(string(input))
}

func main() {
	var highestScore float64
	for y, row := range trees {
		for x, tree := range row {
			score := visiblesToNorth(x, y, tree) *
				visiblesToEast(x, y, tree) *
				visiblesToSouth(x, y, tree) *
				visiblesToWest(x, y, tree)
			highestScore = math.Max(highestScore, float64(score))
		}
	}
	fmt.Println(int(highestScore))
}

func visiblesToNorth(x, y, tree int) (total int) {
	for i := y - 1; i >= 0; i-- {
		total++
		if trees[i][x] >= tree {
			return
		}
	}
	return
}

func visiblesToEast(x, y, tree int) (total int) {
	for i := x + 1; i < len(trees[0]); i++ {
		total++
		if trees[y][i] >= tree {
			return
		}
	}
	return
}

func visiblesToSouth(x, y, tree int) (total int) {
	for i := y + 1; i < len(trees); i++ {
		total++
		if trees[i][x] >= tree {
			return
		}
	}
	return
}

func visiblesToWest(x, y, tree int) (total int) {
	for i := x - 1; i >= 0; i-- {
		total++
		if trees[y][i] >= tree {
			return
		}
	}
	return
}

func format(input string) [][]int {
	var data [][]int
	var row []int

	for _, line := range strings.Split(input, "\n") {
		if line == "" {
			break
		}

		row = make([]int, len(line))
		for i := range line {
			row[i], _ = strconv.Atoi(line[i : i+1])
		}
		data = append(data, row)
	}

	return data
}
