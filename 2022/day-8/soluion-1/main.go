package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

const INPUT = "input.txt"

var trees [][]int

func init() {
	input, err := os.ReadFile(INPUT)
	if err != err {
		panic(err)
	}
	trees = format(string(input))
}

func main() {
	var visibles int
	for y, row := range trees {
		for x, tree := range row {
			if onEdge(x, y) ||
				visibleFromNorth(x, y, tree) ||
				visibleFromEast(x, y, tree) ||
				visibleFromSouth(x, y, tree) ||
				visibleFromWest(x, y, tree) {
				visibles++
				continue
			}
		}
	}
	fmt.Println(visibles)
}

func onEdge(x, y int) bool {
	return x == 0 || x == len(trees[0])-1 || y == 0 || y == len(trees)-1
}

func visibleFromNorth(x, y, tree int) bool {
	for i := y - 1; i >= 0; i-- {
		if trees[i][x] >= tree {
			return false
		}
	}
	return true
}

func visibleFromEast(x, y, tree int) bool {
	for i := x + 1; i < len(trees[0]); i++ {
		if trees[y][i] >= tree {
			return false
		}
	}
	return true
}

func visibleFromSouth(x, y, tree int) bool {
	for i := y + 1; i < len(trees); i++ {
		if trees[i][x] >= tree {
			return false
		}
	}
	return true
}

func visibleFromWest(x, y, tree int) bool {
	for i := x - 1; i >= 0; i-- {
		if trees[y][i] >= tree {
			return false
		}
	}
	return true
}

func format(input string) [][]int {
	var data [][]int
	var row []int

	for _, line := range strings.Split(input, "\n") {
		if line == "" {
			break
		}

		row = make([]int, len(line))
		for i := range line {
			row[i], _ = strconv.Atoi(line[i : i+1])
		}
		data = append(data, row)
	}

	return data
}
