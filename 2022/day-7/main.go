package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const UPDATE_SIZE = 30_000_000
const TOTAL_SPACE = 70_000_000

var fs map[string]*Entry
var command *regexp.Regexp
var dir *regexp.Regexp
var file *regexp.Regexp

type Entry struct {
	Name string
	Size int
	Root string
}

func (e *Entry) UpdateSize(size int) {
	e.Size += size

	root := e.Root
	for {
		entry, ok := fs[root]
		if !ok {
			break
		}

		entry.Size += size
		root = entry.Root
	}
}

func NewDir(name, root string) *Entry {
	return &Entry{Name: name, Root: root}
}

func init() {
	fs = make(map[string]*Entry)
	fs["/"] = NewDir("/", "")
}

func init() {
	var err error

	command, err = regexp.Compile(`^\$`)
	if err != nil {
		panic(err)
	}

	dir, err = regexp.Compile(`^dir`)
	if err != nil {
		panic(err)
	}

	file, err = regexp.Compile(`^\d+`)
	if err != nil {
		panic(err)
	}
}

func main() {
	input, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	root := ""
	directory := ""
	for _, line := range strings.Split(string(input), "\n") {
		if command.MatchString(line) {
			cmd := strings.Replace(line, "$ ", "", -1)
			if cmd == "ls" {
				continue
			}
			params := strings.Split(cmd, " ")
			if params[1] == ".." {
				root = fs[root].Root
				continue
			}
			directory = params[1]
			if root == "" {
				root = directory
			} else {
				root = root + ":" + directory
			}
		}

		if dir.MatchString(line) {
			name := root + ":" + strings.Split(line, " ")[1]
			fs[name] = NewDir(name, root)
		}

		if file.MatchString(line) {
			info := strings.Split(line, " ")
			size, _ := strconv.Atoi(info[0])
			fs[root].UpdateSize(size)
		}
	}

	toDelete := fs["/"].Size
	for _, entry := range fs {
		if TOTAL_SPACE - fs["/"].Size + entry.Size >= UPDATE_SIZE && entry.Size < toDelete {
			toDelete = entry.Size
		}
	}

	fmt.Println(toDelete)
}
